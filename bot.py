import discord
from discord.ext import commands
from discord.utils import get
import json
import matplotlib.pyplot as plot
import requests
import time

settings = json.load(open('settings.json'))
courseEndpoint = "https://api.umd.io/v1/courses/"
departmentEndpoint = "https://api.umd.io/v1/courses/departments"
testudoUrl = "https://app.testudo.umd.edu/soc/"
# explicitly enable privileged intents
intents = discord.Intents.default()
intents.members = True
intents.presences = True
bot = commands.Bot(command_prefix=settings['prefix'], intents=intents)
# remove discord.py's help command because I have my own
bot.remove_command('help')

#
#	Below this point: utilities
#

# adds or removes the placeholder role to the provided list of roles if necessary
def check_roles(guild, roles):
	role = get(guild.roles, name = settings['speechrole'])
	# no change necessary for servers that are not communities
	if role is None:
		return roles
	if role in roles:
		roles.remove(role)
	if len(roles) == 1:
		roles.append(role)
	return roles

# used to override default permissions
def authorized():
	def predicate(ctx):
		return ctx.message.author.id == 68129361588391936
	return commands.check(predicate)

# logs the chat to a guild-specific file
@bot.event
async def on_message(message):
	await bot.process_commands(message)
	with open('logs/' + message.guild.name, 'a+') as log:
		log.write("[" + time.strftime('%Y-%m-%d %H:%M %Z', time.localtime(time.time())) + "][" + message.channel.name + "][" + message.author.name + "] " + message.content + "\n")
	print(message.content)
	return

# assigns a default role to new users so that they can talk in community servers without a verified email
@bot.event
async def on_member_join(member):
	role = get(member.guild.roles, name = settings['speechrole'])
	if role is not None:
		await member.add_roles(role)

#
#	Below this point: user commands
#

# as described
@bot.command()
async def help(ctx):
	await ctx.send("Testudo help menu.\n" +
			"help: Displays this menu.\n" +
			"setmajor [4 letter major code(s delimited with spaces)]: Sets your major. Use without arguments to clear your major. Must be run in the UMD server.\n" +
			"setyear [year]: Sets your graduation year. Use without arguments to clear your graduation year. Must be run in the UMD server.\n" +
			"listmajors: Lists all available majors. If you don't see yours, ask a moderator to add it.\n" +
			"stats: Displays a pie chart of the server's most popular majors.\n" +
			"count [role | \"none\" | \"all\"]: Returns the number of people in the server with the specified role.\n"
			"about: Displays Testudo information.\n" +
			"course [course ID]: Displays course information, including subject, department, credits, and description.\n" +
			"department [department ID]: Displays department name that corresponds with provided department ID.\n"
			"adminhelp: Displays the administrative help menu.")

# assigns a user one or more roles to represent their major
@bot.command()
async def setmajor(ctx, *majors: str):
	author = ctx.message.author
	if not hasattr(author, 'guild'):
		await ctx.send("That command must be run in a server.")
		return
	if False in [major.isalpha() for major in majors]:
		await ctx.send("Invalid format; majors are denoted by the 4 letter course code found at https://app.testudo.umd.edu/soc/.")
		return
	if len(majors) > 4:
		await ctx.send("doubt")
		return
	if len(majors) != len(set(majors)):
		await ctx.send("not what \"double major\" means")
		return
	# check validity of provided roles
	roles = []
	for m in [major.upper() for major in majors]:
		if m in [major for major in settings['majors']]:
			role = get(author.guild.roles, name = m)
			if role is not None:
				roles.append(role)
			else:
				await ctx.send("The major " + m + " exists but lacks a corresponding role. Ask a mod to create one.")
				return
		else:
			# double checks that a ping didn't slip through
			await ctx.send("No such major" + ("" if "@" in m else " \"" + m + "\"") + ".")
			return
	# remove all majors, add new ones
	keep = author.roles
	previous = []
	for role in author.roles:
		if role.name in [major for major in settings['majors']]:
			previous.append(role)
			keep.remove(role)
	keep += roles
	keep = check_roles(ctx.guild, keep)
	await author.edit(roles=keep)
	# list the majors added in a user-friendly string
	messagestr = "I set your major"
	if len(roles) > 1:
		messagestr += "s to "
		for i in range(0, len(roles)):
			messagestr += roles[i].name
			if i < len(roles) - 1:
				if len(roles) > 2:
					messagestr += ", "
				else:
					messagestr += " "
			if i == len(roles) - 2:
				messagestr += "and "
	elif len(roles) == 1:
		messagestr += " to " + roles[0].name
	else:
		messagestr = "I cleared your major"
		if len(previous) > 1:
			messagestr += "s"
		elif len(previous) == 0:
			messagestr = "I neither cleared nor set any new majors"
	messagestr += ", " + author.name + "."
	# warn the user that prior majors were erased, if applicable
	secondstr = ""
	if len(previous) > 0 and len(set(roles) & set(previous)) == 0 and len(roles) > 0:
		secondstr = (" Your old major" +
				(" was" if len(previous) == 1 else "s were") +
				" overwritten. If you had intended to set " +
				("an additional major" if len(roles) == 1 else "additional majors") +
				", use " +
				settings['prefix'] + "setmajor " + " ".join(role.name for role in previous + roles) + ".")
	await ctx.send(messagestr + secondstr)

# assigns a user a role to represent their graudation year
@bot.command()
async def setyear(ctx, *year: str):
	year = " ".join(year)
	author = ctx.message.author
	if not hasattr(author, 'guild'):
		await ctx.send("That command must be run in a server.")
		return
	if year and not year.isdigit():
		await ctx.send("That's not a year.")
		return
	role = None
	# determine which roles to keep
	keep = author.roles
	removed = False
	for r in author.roles:
		if r.name in [year for year in settings['years']]:
			keep.remove(r)
			removed = True
	if year != "":
		if year in [year for year in settings['years']]:
			role = get(author.guild.roles, name = year)
			if role is None:
				await ctx.send("The year " + year + " exists but lacks a corresponding role. Ask a mod to create one.")
				return
			else:
				keep.append(role)
		else:
			# double checks that a ping didn't slip through
			await ctx.send("No such year" + ("" if "@" in year else " \"" + year + "\"") + ".")
			return
	keep = check_roles(ctx.guild, keep)
	await author.edit(roles=keep)
	# inform the user of the changes that took place
	messagestr = ""
	if role is not None:
		messagestr = "I " + ("changed" if removed else "set") + " your year to " + role.name
	elif removed:
		messagestr = "I cleared your year"
	else:
		messagestr = "I neither cleared nor set your year"
	await ctx.send(messagestr + ", " + author.name + ".")

# as described
@bot.command()
async def listmajors(ctx):
	await ctx.send("Majors are denoted by their 4 letter course code (e.g. \"CMSC\" = Computer Science).\nA complete list can be found at https://app.testudo.umd.edu/soc/.")

# generates and displays as an embed a pie chart containing the server's most common majors
@bot.command()
async def stats(ctx):
	majors = {}
	total_majors = 0
	for member in ctx.guild.members:
		for role in member.roles:
			if role.name in [role for role in settings['majors']]:
				total_majors += 1
				if role.name in majors:
					majors[role.name] += 1
				else:
					majors[role.name] = 1

	labels = []
	sizes = []
	other = 0
	for major in majors:
		if majors[major] / total_majors > .01:
			labels.append(major)
			sizes.append(majors[major])
		else:
			other += majors[major]
	sizes, labels = (list(s) for s in zip(*sorted(zip(sizes, labels))))
	if (other > 0):
		labels.insert(0, 'other')
		sizes.insert(0, other)
	for i in range(0, len(labels)):
		labels[i] += " (" + str(round(100 * sizes[i] / total_majors, 2)) + "%)"

	patches, texts = plot.pie(sizes, shadow=False, startangle=90)
	plot.legend(patches[::-1], labels[::-1], bbox_to_anchor=(1.1,0.5), loc="right", bbox_transform=plot.gcf().transFigure)
	plot.axis('equal')
	plot.savefig('temp.png', bbox_inches="tight")
	plot.close()

	embed = discord.Embed(title="Distribution of majors in the " + ctx.guild.name + " Discord server", color=0xf1c40f)
	file = discord.File('temp.png', filename="image.png")
	embed.set_image(url="attachment://image.png")
	await ctx.send(file=file, embed=embed)

# counts the number of users who have the provided role
@bot.command()
async def count(ctx, *role_str: str):
	role_str = " ".join(role_str)
	if role_str.lower() == "none":
		count = 0
		for member in ctx.guild.members:
			if len(member.roles) == 1:
				count += 1
		await ctx.send(str(count) + (" user has" if count == 1 else " users have") + " no role.")
	elif role_str.lower() == "all":
		count = 0
		for member in ctx.guild.members:
			count += 1
		await ctx.send(str(count) + (" user is" if count == 1 else " users are") + " in this server.")
	else:
		# if getting the role doesn't work, check to see if the uppercase version exists
		# people frequently try to count the number of majors but pass the lowercase version of the role
		role = get(ctx.guild.roles, name=role_str) or get(ctx.guild.roles, name=role_str.upper())
		if role is None:
			await ctx.send("No such role.")
			return
		count = 0
		for member in ctx.guild.members:
			if role in member.roles:
				count += 1
		await ctx.send(str(count) + " users have " + ("the " + role.name if role.name[0] != "@" else "that") + " role.")

# as described
@bot.command()
async def about(ctx):
	await ctx.send("Testudo: A bot for the UMD discord server.\nSource code available at https://gitlab.com/nahkoots/umd-discord-bot.")

# generates an embed containing data about a provided course
@bot.command()
async def course(ctx, *message: str):
	message = (" ".join(message)).replace(" ", "") # in case people use "course cmsc 216" or equivalent
	r = requests.get(courseEndpoint + message).json()
	if not 'error_code' in r:
		r = r[0]
		em = discord.Embed(
				title=r['course_id'] + ": " + r['name'],
				color=discord.Color.from_rgb(226, 24, 51),
				url=testudoUrl + r['semester']+"/"+r['dept_id']+"/"+r['course_id'])
		em.add_field(name="Description", value=r['description'] or "No description provided")
		em.add_field(name="Department", value=r['department'], inline=False)
		em.add_field(name="Prerequisites", value=r['relationships']['prereqs'] or "None", inline=False)
		em.add_field(name="Course ID", value=r['course_id'], inline=True)
		em.add_field(name="Credits", value=r['credits'], inline=True)
		await ctx.send(embed=em)
	elif r['error_code'] == 400:
		await ctx.send("Invalid format: course IDs take the form [major code][course number] (for example: CMSC132).")
	elif r['error_code'] == 404:
		await ctx.send("No such course.")

# expands a department's abbreviation
@bot.command()
async def department(ctx, *message: str):
	message = (" ".join(message)).upper()
	r = requests.get(departmentEndpoint).json()
	r = {i['dept_id']: i['dept_id'] + ": " + i['department'] for i in r}
	await ctx.send(r.get(message, "No such department."))

# allows a user to opt out of the politics chat
@bot.command()
async def apolitical(ctx):
	role = get(ctx.guild.roles, name = 'apolitical')
	if role is None:
		await ctx.send("No politics channel to opt out of.")
	if role in ctx.author.roles:
		await ctx.author.remove_roles(role)
		await ctx.send("Opted you back into the politics chat.")
	else:
		await ctx.author.add_roles(role)
		await ctx.send("Opted you out of the politics chat.")


#
#	Below this point: administrative commands
#

# as described
@bot.command()
async def adminhelp(ctx):
	await ctx.send("Testudo administrative help menu.\n" +
			"voice: For use in community servers. Assigns the default role (\".\") to users who have no other roles.\n" +
			"backup: Saves each user's list of roles to a file.\n" +
			"populate: Creates all of the major and year roles for the server.\n" +
			"depopulate: Deletes all of the major and year roles.\n" +
			"delrole [role]: Deletes all copies of the provided role.\n" +
			"restrict: Removes some default permissions from all major and year roles.")

# assigns the default role to users who need it and do not already have it
@bot.command()
@commands.check_any(commands.has_permissions(manage_roles=True), authorized())
async def voice(ctx):
	role = get(ctx.guild.roles, name = settings['speechrole'])
	for member in ctx.guild.members:
		if len(member.roles) == 1:
			await member.add_roles(role)
	await ctx.send("Done.")

# iterates over users and saves all of their roles to a file
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def backup(ctx):
	userroles = {}
	for member in ctx.guild.members:
		roles = [role.name for role in member.roles]
		userroles[member.id] = roles
	with open('backups/' + ctx.guild.name, 'a+') as backup:
		json.dump(userroles, backup)
	await ctx.send("Done.")

# creates any of the major and year roles for the server that do not already exist
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def populate(ctx):
	for role_str in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role_str)
		if role is None:
			await ctx.guild.create_role(name=role_str)
	await ctx.send("Done.")

# deletes all of the major and year roles for the server
# use with caution
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def depopulate(ctx, confirm: str=None):
	if (confirm != "confirm"):
		await ctx.send("Type " + settings['prefix'] + "depopulate confirm to run this command.")
		return
	for role in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role)
		if role is not None:
			await role.delete()
	await ctx.send("Done.")

# deletes all instances of a role
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def delrole(ctx, *role_str: str):
	role_str = " ".join(role_str)
	role = get(ctx.guild.roles, name=role_str)
	while role is not None:
		await role.delete()
		role = get(ctx.guild.roles, name=role_str)
	await ctx.send("Done.")

# removes some of the permissions from each year/major role
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def restrict(ctx):
	for role in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role)
		if role is not None:
			p = role.permissions
			p.send_tts_messages = False
			p.embed_links = False
			p.attach_files = False
			await role.edit(permissions=p)
	await ctx.send("Done.")

# repeats the last message sans command
@bot.command()
@authorized()
async def say(ctx, *message: str):
	message = " ".join(message)
	await ctx.send(message)
	await ctx.message.delete()

bot.run(json.load(open('auth.json'))['token'])
